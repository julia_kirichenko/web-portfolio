// total counter (euro amount at the bottom)
function readDropZone() {
    var total = document.getElementById('price');
    var dough = document.getElementById('dough').children;
  
    for(var i = 0; i < dough.length; i++) {
        var hitdough = dough[i];
        var price = hitdough.getAttribute('data-price');
        var subTotal = parseFloat(price);  
        }
        total.innerHTML = (parseFloat(total.innerHTML) + parseFloat(subTotal));
        total.innerHTML = parseFloat(total.innerHTML).toFixed(2)
    }

function dragstart_handler(ev) {
    ev.dataTransfer.setData("application", ev.target.id);
    ev.dataTransfer.dropEffect = "copy";
   }

function dragover_handler(ev) {
    ev.preventDefault();
    ev.dataTransfer.dropEffect = "copy";
   }

function drop_handler(ev) {
    ev.preventDefault();
    const data = ev.dataTransfer.getData("application");	

    // if drop event happened on the img container
    if (ev.target == document.getElementById("dough") ) {
        copyNode( data, ev.target );
    }
    // if drop event happened on one of the imgs in the container
    else if ( ev.target.parentElement == document.getElementById("dough") ) {
          copyNode( data, ev.target.parentElement );
    }
    readDropZone();
   }
//copying a source image for drag'n'drop (in order to not leave an empty space on ingredient list instead)
function copyNode( id, parent ) {
    var nodeCopy = document.getElementById(id).cloneNode(true);
    nodeCopy.id = id + "_copy";
    parent.appendChild( nodeCopy );
};
