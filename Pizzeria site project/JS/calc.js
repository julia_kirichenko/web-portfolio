// radio buttons counter (using jQuery library)
    $(document).ready(function ($) {
	$("#pizza").change(function() {
		
		var totalPrice   = 0;
            values       = [];
        
		$('input[type=radio]').each(function() {
            if( $(this).is(':checked') ) {
                values.push($(this).val());
				totalPrice += parseFloat($(this).val());
            }
        });

		$("#price").text(totalPrice.toFixed(2));	
	});
});

